import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { SoutenanceEntity } from '../../soutenance/entities/Soutenance.entity';


  @Entity('room')
  export class RoomEntity {

    @PrimaryGeneratedColumn()
    id: string;
  
    @Column()
    name:string;

    @OneToMany(
      type => RoomEntity,
      (room) => room.soutenancesList,
      {lazy: true}
   )
    soutenancesList: SoutenanceEntity;

}