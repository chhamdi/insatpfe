import { JwtAuthGuard } from './../../user/Guards/jwt-auth.guard';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { RoomEntity } from '../entities/Room.entity';
import { RoomService } from '../services/room.service';
import { RoomDto } from '../DTO/room.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';
import { ApiTags } from '@nestjs/swagger';
import { Timestamp } from 'typeorm';

@ApiTags('salles')
@Controller('salles')
export class RoomController {
  constructor(private roomService: RoomService) {}

  @Post()
  //@UseGuards(JwtAuthGuard)
  async addRoom(@Body() newRoom: RoomDto): Promise<RoomEntity> {
    return await this.roomService.create(newRoom);
  }

  @Get(':id')
  async getRoom(@Param('id') idRoom: string) {
    return await this.roomService.findRoomById(idRoom);
  }
  @Delete(':id')
  //@UseGuards(JwtAuthGuard)
  deleteRoom(@Param('id') id: string) {
    return this.roomService.delete(id);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.roomService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 10 ? 10 : paginationDto.limit,
    });
  }

  @Get('/disponibles/:date')
  async GetAvailableRoomForDate(@Param('date') soutenanceDate: Timestamp) {
    console.log(soutenanceDate);
    return this.roomService.GetAvailableRoomForDate(soutenanceDate);
  }
}
