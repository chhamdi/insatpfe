import { SoutenanceService } from './../soutenance/services/soutenance.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomEntity } from './entities/Room.entity';
import { RoomService } from './services/room.service';
import { RoomController } from './controllers/room.controller';
import { SoutenanceEntity } from 'src/soutenance/entities/Soutenance.entity';

@Module({
  controllers: [RoomController],
  providers: [RoomService, SoutenanceService],
  imports: [TypeOrmModule.forFeature([RoomEntity, SoutenanceEntity])],
})
export class RoomModule {}
