import { SoutenanceEntity } from './../../soutenance/entities/Soutenance.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RoomEntity } from '../entities/Room.entity';
import { Repository, Timestamp } from 'typeorm';
import { RoomDto } from '../DTO/room.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';

@Injectable()
export class RoomService {
  constructor(
    @InjectRepository(RoomEntity)
    private roomRepository: Repository<RoomEntity>,
    @InjectRepository(SoutenanceEntity)
    private soutenaceRepository: Repository<SoutenanceEntity>,
  ) {}

  async create(newRoom: RoomDto): Promise<RoomEntity> {
    return await this.roomRepository.save(newRoom);
  }

  async findRoomById(id: string) {
    return await this.roomRepository.findOne(id);
  }

  async delete(id: string) {
    return await this.roomRepository.delete(id);
  }

  async findAll(paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;

    const totalCount = await this.roomRepository.count();
    const rooms = await this.roomRepository
      .createQueryBuilder()
      .orderBy('id', 'DESC')
      .offset(skippedItems)
      .limit(paginationDto.limit)
      .getMany();

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      data: rooms,
    };
  }
  //SELECT * FROM ROOM WHERE id NOT IN (SELECT `roomId` FROM `soutenance` WHERE '2021-01-28 07:50:57' BETWEEN `soutenanceStartTime` AND `soutenanceEndTime`)
  async GetAvailableRoomForDate(soutenanceDate: Timestamp) {
    //idRoomQb=SELECT `roomId` FROM `soutenance` WHERE soutenanceDate BETWEEN `soutenanceStartTime` AND `soutenanceEndTime`
    const idRoomQb = await this.soutenaceRepository
      .createQueryBuilder()
      .select('roomId')
      .where(':date BETWEEN soutenanceStartTime AND soutenanceEndTime ', {
        date: soutenanceDate,
      });
    //rooms=SELECT * FROM ROOM WHERE ID NOT IN q1
    const rooms = await this.roomRepository
      .createQueryBuilder()
      .where('id NOT IN (' + idRoomQb.getQuery() + ')')
      .setParameters(idRoomQb.getParameters())
      .getMany();

    return {
      rooms: rooms,
    };
  }
}
