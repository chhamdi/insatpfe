import { ApiProperty } from '@nestjs/swagger';

export class RoomDto {
  @ApiProperty({
    type: String,
    description: 'libellé de la salle de soutenance',
  })
  name: string;
}
