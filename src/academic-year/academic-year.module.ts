import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AcademicYearEntity } from './entities/AcademicYear.entity';
import { AcademicYearService } from './services/academic-year.service';
import { AcademicYearController } from './controllers/academic-year.controller';

@Module({
  controllers: [AcademicYearController],
  providers: [AcademicYearService],
  imports: [
    TypeOrmModule.forFeature([
        AcademicYearEntity
    ])
  ]
})
export class AcademicYearModule {}