import {Entity, PrimaryGeneratedColumn, OneToMany, Column } from 'typeorm';
import { SessionEntity } from '../../session/entities/Session.entity';
import { SoutenanceEntity } from '../../soutenance/entities/Soutenance.entity';
import { StudentEntity } from '../../student/entities/Student.entity';


  @Entity('academic_year')
export class AcademicYearEntity {

    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    name : string;

    @OneToMany(
        type => SessionEntity,
        (session) => session.academicYear, 
        {lazy: true}       
    )
    sessionList: SoutenanceEntity [];

    @OneToMany(
        type => StudentEntity,
        (student) => student.academicYear,
        {lazy: true}
    )
    studentList:StudentEntity[];
  
}