import { ApiProperty } from '@nestjs/swagger';

export class AcademicYearDto {
  @ApiProperty({
    type: String,
    description: "le nom d'une année universitaire",
  })
  name: string;
}
