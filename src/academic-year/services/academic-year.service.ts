import { Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AcademicYearEntity } from '../entities/AcademicYear.entity';
import { Repository } from 'typeorm';
import { AcademicYearDto } from '../DTO/academicYear.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';

@Injectable()
export class AcademicYearService {
  constructor(
    @InjectRepository(AcademicYearEntity)
    private academicYearRepository: Repository<AcademicYearEntity>,
  ) {}

  async create(newAcademicYear: AcademicYearDto): Promise<AcademicYearEntity> {
    return await this.academicYearRepository.save(newAcademicYear);
  }

  async findAcademicYearById(id: number) {
    return await this.academicYearRepository.findOne(id);
  }

  async delete(id: number) {
    return await this.academicYearRepository.delete(id);
  }

  async findAll(paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;

    const totalCount = await this.academicYearRepository.count();
    const academicYears = await this.academicYearRepository
      .createQueryBuilder()
      .orderBy('id', 'DESC')
      .offset(skippedItems)
      .limit(paginationDto.limit)
      .getMany();

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      data: academicYears,
    };
  }
}
