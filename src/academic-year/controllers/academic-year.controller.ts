import {
  Body,
  Controller,
  Post,
  Get,
  Param,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AcademicYearService } from '../services/academic-year.service';
import { AcademicYearDto } from '../DTO/academicYear.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';
import { JwtAuthGuard } from 'src/user/Guards/jwt-auth.guard';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('annees_universitaires')
@Controller('annees_universitaires')
export class AcademicYearController {
  constructor(private academicYearService: AcademicYearService) {}

  @Post()
  //@UseGuards(JwtAuthGuard)
  async addAcademicYear(@Body() newAcademicYear: AcademicYearDto) {
    return await this.academicYearService.create(newAcademicYear);
  }

  @Get(':id')
  async readAcademicYear(@Param('id') idAcademicYear: number) {
    return await this.academicYearService.findAcademicYearById(idAcademicYear);
  }

  @Delete(':id')
  //@UseGuards(JwtAuthGuard)
  deleteAcademicYear(@Param('id') id: number) {
    return this.academicYearService.delete(id);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    paginationDto.page = paginationDto.page;
    paginationDto.limit = paginationDto.limit;

    return this.academicYearService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 10 ? 10 : paginationDto.limit,
    });
  }

  
}
