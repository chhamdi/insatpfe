import { SoutenanceService } from './../soutenance/services/soutenance.service';
import { SoutenanceEntity } from './../soutenance/entities/Soutenance.entity';
import { InternshipService } from './../internship/services/internship.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeacherController } from './controllers/teacher.controller';
import { TeacherEntity } from './entities/Teacher.entity';
import { TeacherService } from './services/teacher.service';
import { InternshipEntity } from 'src/internship/entities/Internship.entity';

@Module({
  controllers: [TeacherController],
  providers: [TeacherService, InternshipService, SoutenanceService],
  imports: [
    TypeOrmModule.forFeature([
      TeacherEntity,
      InternshipEntity,
      SoutenanceEntity,
    ]),
  ],
})
export class TeacherModule {}
