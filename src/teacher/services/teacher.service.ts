import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TeacherEntity } from '../../teacher/entities/Teacher.entity';
import { Repository } from 'typeorm';
import { TeacherDto } from '../../teacher/DTO/teacher.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';

@Injectable()
export class TeacherService {
  constructor(
    @InjectRepository(TeacherEntity)
    private teacherRepository: Repository<TeacherEntity>,
  ) {}

  async create(newTeacher: TeacherDto): Promise<TeacherEntity> {
    return await this.teacherRepository.save(newTeacher);
  }

  async findTeacherById(id: string) {
    return await this.teacherRepository.findOne(id);
  }
  async delete(id: string) {
    return await this.teacherRepository.delete(id);
  }

  async findAll(paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;

    const totalCount = await this.teacherRepository.count();
    const teachers = await this.teacherRepository
      .createQueryBuilder()
      .orderBy('id', 'DESC')
      .offset(skippedItems)
      .limit(paginationDto.limit)
      .getMany();

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      data: teachers,
    };
  }
}
