import { SoutenanceService } from './../../soutenance/services/soutenance.service';
import { ApiTags } from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { TeacherService } from '../services/teacher.service';
import { TeacherDto } from '../../teacher/DTO/teacher.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';
import { JwtAuthGuard } from 'src/user/Guards/jwt-auth.guard';
import { InternshipService } from 'src/internship/services/internship.service';

@ApiTags('enseignants')
@Controller('enseignants')
export class TeacherController {
  constructor(
    private teacherService: TeacherService,
    private internshipService: InternshipService,
    private soutenanceService: SoutenanceService,
  ) {}

  @Post()
  //@UseGuards(JwtAuthGuard)
  async addTeacher(@Body() newTeacher: TeacherDto) {
    return await this.teacherService.create(newTeacher);
  }

  @Get(':id')
  async getTeacher(@Param('id') idTeacher: string) {
    return await this.teacherService.findTeacherById(idTeacher);
  }

  @Delete(':id')
  //@UseGuards(JwtAuthGuard)
  deleteTeacher(@Param('id') id: string) {
    return this.teacherService.delete(id);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.teacherService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 10 ? 10 : paginationDto.limit,
    });
  }
  @Get(':id/stages')
  async GetListOfTeacherInternship(@Param('id') id: string) {
    return this.internshipService.GetListOfTeacherInternship(id);
  }

  @Get(':id/soutenances/president')
  async GetListOfSoutenanceWhereTeacherIsPresident(@Param('id') id: string) {
    return this.soutenanceService.GetListOfSoutenanceWhereTeacherIsPresident(
      id,
    );
  }

  @Get(':id/soutenances/examinateur')
  async GetListOfSoutenanceWhereTeacherIsReview(@Param('id') id: string) {
    return this.soutenanceService.GetListOfSoutenanceWhereTeacherIsReview(id);
  }
}
