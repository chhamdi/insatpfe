import { SessionModule } from './session/session.module';
import { RoomModule } from './room/room.module';
import { InternshipModule } from './internship/internship.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import * as dotenv from 'dotenv';
import { UserModule } from './user/user.module';
import { AcademicYearModule } from './academic-year/academic-year.module';
import { SoutenanceModule } from './soutenance/soutenance.module';
import { StudentModule } from './student/student.module';
import { TeacherModule } from './teacher/teacher.module';
import { join } from 'path';

dotenv.config();

@Module({
  imports: [
    AcademicYearModule,
    InternshipModule,
    RoomModule,
    SessionModule,
    SoutenanceModule,
    StudentModule,
    TeacherModule,
    UserModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [join(__dirname, './**/**/entities/*.entity{.ts,.js}')],
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
