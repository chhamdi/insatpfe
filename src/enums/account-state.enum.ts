export enum AccountStateEnum {
    ACTIVATED = 'active',
    DISABLED = 'désactivé',
}