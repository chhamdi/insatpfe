import { SessionEntity } from './../entities/Session.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SessionDto } from 'src/DTO/session.dto';

@Injectable()
export class SessionService {
    constructor(
        @InjectRepository(SessionEntity)
        private sessionRepository: Repository<SessionEntity>
    ){}
    
    async create(newSession: SessionDto): Promise<SessionEntity> {
        return await this.sessionRepository.save(newSession);
    }

    async findSessionById(id: string) {
        return await this.sessionRepository.findOne(id);
   } 
}
