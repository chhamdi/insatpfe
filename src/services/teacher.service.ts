import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TeacherEntity } from 'src/entities/Teacher.entity';
import { Repository } from 'typeorm';
import { TeacherDto } from '../DTO/teacher.dto';

@Injectable()
export class TeacherService {
    
    constructor(
        @InjectRepository(TeacherEntity)
        private teacherRepository: Repository<TeacherEntity>
    ){}
        
    async create(newTeacher: TeacherDto): Promise<TeacherEntity> {
        return await this.teacherRepository.save(newTeacher);
    }

    async findTeacherById(id: string) {
        return await this.teacherRepository.findOne(id);
   } 
}
