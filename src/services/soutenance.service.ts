import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SoutenanceEntity } from 'src/entities/Soutenance.entity';
import { Repository } from 'typeorm';
import { SoutenanceDto } from '../DTO/soutenance.dto';

@Injectable()
export class SoutenanceService {
    
    constructor(
        @InjectRepository(SoutenanceEntity)
        private soutenanceRepository: Repository<SoutenanceEntity>
    ){}
        
    async create(newSoutenance: SoutenanceDto): Promise<SoutenanceEntity> {
        return await this.soutenanceRepository.save(newSoutenance);
    }

    async findSoutenanceById(id: string) {
        return await this.soutenanceRepository.findOne(id);
   } 
}
