import { Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StudentEntity } from 'src/entities/Student.entity';
import { Repository } from 'typeorm';
import { StudentDto } from '../DTO/student.dto';

@Injectable()
export class StudentService {

    constructor(
        @InjectRepository(StudentEntity)
        private studentRepository: Repository<StudentEntity>
    ){}
        
    async create(newStudent: StudentDto): Promise<StudentEntity> {
        return await this.studentRepository.save(newStudent);
    }

    async findStudentById(id: string) {
        return await this.studentRepository.findOne(id);
   } 

}
