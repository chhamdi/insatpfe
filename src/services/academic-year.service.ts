import { Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AcademicYearEntity } from 'src/entities/AcademicYear.entity';
import { Repository } from 'typeorm';
import { AcademicYearDto } from '../DTO/academicYear.dto';

@Injectable()
export class AcademicYearService {
    
    constructor(
        @InjectRepository(AcademicYearEntity)
        private academicYearRepository: Repository<AcademicYearEntity>
    ){}
        
    async create(newAcademicYear: AcademicYearDto): Promise<AcademicYearEntity> {
        return await this.academicYearRepository.save(newAcademicYear);
    }

    async findAcademicYearById(id: string) {
        return await this.academicYearRepository.findOne(id);
    }  
}
