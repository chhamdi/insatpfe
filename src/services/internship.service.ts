import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InternshipEntity } from 'src/entities/Internship.entity';
import { Repository } from 'typeorm';
import { InternshipDto } from '../DTO/internship.dto';

@Injectable()
export class InternshipService {
     
    constructor(
        @InjectRepository(InternshipEntity)
        private internshipRepository: Repository<InternshipEntity>
    ){}
      
    async findAllInternship() {
        return await this.internshipRepository.find()
    }

    async create(newInternship: InternshipDto): Promise<InternshipEntity> {
        return await this.internshipRepository.save(newInternship);
    }

    async findInternshipById(id: string) {
         return await this.internshipRepository.findOne(id);
    }  
}
