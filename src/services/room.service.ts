import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RoomEntity } from 'src/entities/Room.entity';
import { Repository } from 'typeorm';
import { RoomDto } from '../DTO/room.dto';

@Injectable()
export class RoomService {

    constructor(
        @InjectRepository(RoomEntity)
        private roomRepository: Repository<RoomEntity>
    ){}
    
    async create(newRoom: RoomDto): Promise<RoomEntity> {
        return await this.roomRepository.save(newRoom);
    }

    async findRoomById(id: string) {
        return await this.roomRepository.findOne(id);
   } 
}
  