import { AcademicYearDto } from './academicYear.dto';

export class SessionDto {

  startDate: Date;
  endDate: Date;
  academicYear: AcademicYearDto;

}