import { AcademicYearDto } from "./academicYear.dto";
import { UserDto } from './user.dto';



export class StudentDto extends UserDto {
  
  inscription_number: number;
  section: string;
  branch: string;
  academicYear: AcademicYearDto;
}