import { StudentDto } from './student.dto';
import { TeacherDto } from './teacher.dto';


export class InternshipDto {

  student: StudentDto;
  topic: string;
  report: string;

  society: string;

  societySupervisor: string;

  schoolSupervisor1:TeacherDto;
  schoolSupervisor2:TeacherDto;

  startDate: Date;
  endDate: Date;
  state: string;

}