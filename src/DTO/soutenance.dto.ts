import { InternshipDto } from './internship.dto';
import { RoomDto } from './room.dto';
import { SessionDto } from './session.dto';
import { TeacherDto } from './teacher.dto';

export class SoutenanceDto {

    session: SessionDto;  
    internship: InternshipDto;

    president: TeacherDto;
    reviewer: TeacherDto;

    schoolSupervisor1: TeacherDto;
    schoolSupervisor2: TeacherDto;

    soutenanceStartTime: Date;
    soutenanceEndTime: Date;
    room: RoomDto;
    
}