import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentController } from './controllers/student.controller';
import { StudentEntity } from './entities/Student.entity';
import { StudentService } from './services/student.service';


@Module({
  controllers: [StudentController],
  providers: [StudentService],
  imports: [TypeOrmModule.forFeature([StudentEntity])],
})
export class StudentModule {}