import { Entity, Column, OneToMany, ManyToMany, ManyToOne } from 'typeorm';
import { UserEntity } from '../../user/entities/User.entity';
import { InternshipEntity} from '../../internship/entities/Internship.entity';
import { SectionEnum } from './../enums/section-etudiant.enum';
import { AcademicYearEntity } from '../../academic-year/entities/AcademicYear.entity';


  @Entity('student')
  export class StudentEntity extends UserEntity{
    
    @Column()
    inscription_number: number;

    @Column({
        type: 'enum',
        enum: SectionEnum,
        default: SectionEnum.INGENIEURIE
      })
    section: string;

    @Column()
    branch: string;

    @OneToMany(
      type => InternshipEntity,
      (internship) => internship.student,
      {eager: true}
    )
    internshipList: InternshipEntity[];

    @ManyToOne(
      type => AcademicYearEntity,
      (year) => year.sessionList,
      {eager: true}
    )
    academicYear: AcademicYearEntity;
  

  }

