import { AcademicYearService } from './../../services/academic-year.service';
import { AcademicYearController } from './../../controllers/academic-year.controller';
import { JwtAuthGuard } from './../../user/Guards/jwt-auth.guard';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { SoutenanceEntity } from '../../soutenance/entities/Soutenance.entity';
import { StudentService } from '../services/student.service';
import { StudentEntity } from '../entities/Student.entity';
import { StudentDto } from '../DTO/student.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';
import { ApiTags } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import * as XLSX from 'xlsx';


@ApiTags('etudiants')
@Controller('etudiants')
export class StudentController {
  constructor(private studentService: StudentService) {}

  @Post('/import')
  @UseInterceptors(FileInterceptor('file'))
  async addStudentFromExcel(@UploadedFile() file, @Body() body) {
    const workbook = XLSX.read(file.buffer);
    var sheetNames = workbook.SheetNames;
    var sheetIndex = 1;
    var df = XLSX.utils.sheet_to_json(workbook.Sheets[sheetNames[sheetIndex-1]]);
    const studentsToAdd = [];
    const currentStudents = (await this.findAll({page: 1, limit:5000})).data;
    df.forEach(student => {
      let s = currentStudents.find(s => s.inscription_number == student["inscription_number"])
      if(!s) {
        student["academicYear"] = body['academicYear'];
        student["password"] = Math.random().toString(36).slice(-8);
        studentsToAdd.push(student);
      }
    })
    
    return await this.studentService.createMany(studentsToAdd);

  }
  

  @Post()
  //@UseGuards(JwtAuthGuard)
  async addStudent(@Body() newStudent: StudentDto) {
    return await this.studentService.create(newStudent);
  }

  @Get(':id')
  async getStudent(@Param('id') idStudent: string): Promise<StudentEntity> {
    return await this.studentService.findStudentById(idStudent);
  }

  @Get(':id/stages')
  async getListOfInternshipOfStudent(@Param('id') idStudent: string) {
    const student = await this.studentService.findStudentById(idStudent);
    return student.internshipList;
  }

  @Get(':id/annee')
  async getYearOfStudent(@Param('id') idStudent: string) {
    const student = await this.studentService.findStudentById(idStudent);
    return student.academicYear;
  }
  @Get(':id/soutenances')
  async getListOfSoutenanceOfStudent(@Param('id') idStudent: string) {
    const listOfInternships = await this.getListOfInternshipOfStudent(
      idStudent,
    );
    const listOfSoutenances: SoutenanceEntity[] = [];
    for (const internship of listOfInternships) {
      if (internship.soutenance != null) {
        listOfSoutenances.push(internship.soutenance);
      }
    }
    return listOfSoutenances;
  }

  @Delete(':id')
  //@UseGuards(JwtAuthGuard)
  deleteStudent(@Param('id') id: string) {
    return this.studentService.delete(id);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.studentService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 10 ? 10 : paginationDto.limit,
    });
  }

  @Put(':id')
  //@UseGuards(JwtAuthGuard)
  updateStudent(
    @Body() newStudent: StudentDto,
    @Param('id') id: string,
  ): Promise<StudentEntity> {
    return this.studentService.update(id, newStudent);
  }
}
