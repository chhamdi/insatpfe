import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StudentEntity } from '../entities/Student.entity';
import { Repository } from 'typeorm';
import { StudentDto } from '../DTO/student.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';

@Injectable()
export class StudentService {
  constructor(
    @InjectRepository(StudentEntity)
    private studentRepository: Repository<StudentEntity>,
  ) {}

  async createMany(newStudents: StudentDto[]): Promise<StudentEntity[]> {
    return await this.studentRepository.save(newStudents);
  }

  async create(newStudent: StudentDto): Promise<StudentEntity> {
    return await this.studentRepository.save(newStudent);
  }

  async findStudentById(id: string) {
    return await this.studentRepository.findOne(id);
  }
  async delete(id: string) {
    return await this.studentRepository.delete(id);
  }

  async findAll(paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;

    const totalCount = await this.studentRepository.count();
    const students = await this.studentRepository
      .createQueryBuilder()
      .orderBy('id', 'DESC')
      .offset(skippedItems)
      .limit(paginationDto.limit)
      .getMany();

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      data: students,
    };
  }

  async update(id: string, newStudent: StudentDto): Promise<StudentEntity> {
    const student = await this.studentRepository.preload({
      id,
      ...newStudent,
    });
    console.log(student);
    if (!student) {
      new NotFoundException(`Le student d'id ${id} n'existe pas`);
    }
    return await this.studentRepository.save(student);
  }
}
