import { ApiProperty } from '@nestjs/swagger';
import { AcademicYearDto } from '../../academic-year/DTO/academicYear.dto';
import { UserDto } from '../../user/DTO/user.dto';

export class StudentDto extends UserDto {
  @ApiProperty({
    type: Number,
    description: "le numéro d'inscription",
  })
  inscription_number: number;

  @ApiProperty({
    type: String,
    description: 'la section',
  })
  section: string;

  @ApiProperty({
    type: String,
    description: 'la branche',
  })
  branch: string;

  @ApiProperty({
    type: Number,
    description: "l'année académique",
  })
  academicYear: AcademicYearDto;
}
