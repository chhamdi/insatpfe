import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InternshipController } from './controllers/internship.controller';
import { InternshipService } from './services/internship.service';
import { InternshipEntity } from './entities/Internship.entity';

@Module({
  controllers: [InternshipController],
  providers: [InternshipService],
  imports: [
    TypeOrmModule.forFeature([
        InternshipEntity
    ])
  ]
})
export class InternshipModule {}