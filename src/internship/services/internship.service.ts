import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InternshipEntity } from '../entities/Internship.entity';
import { Repository } from 'typeorm';
import { InternshipDto } from '../DTO/internship.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';

@Injectable()
export class InternshipService {
  constructor(
    @InjectRepository(InternshipEntity)
    private internshipRepository: Repository<InternshipEntity>,
  ) {}

  async findAllInternship() {
    return await this.internshipRepository.find();
  }

  async create(newInternship: InternshipDto): Promise<InternshipEntity> {
    return await this.internshipRepository.save(newInternship);
  }

  async findInternshipById(id: string) {
    return await this.internshipRepository.findOne(id);
  }

  async delete(id: string) {
    return await this.internshipRepository.delete(id);
  }

  async findAll(paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;

    const totalCount = await this.internshipRepository.count();
    const internships = await this.internshipRepository
      .createQueryBuilder()
      .orderBy('id', 'DESC')
      .offset(skippedItems)
      .limit(paginationDto.limit)
      .getMany();

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      data: internships,
    };
  }

  async GetListOfTeacherInternship(idTeacher: string) {
    const interships = await this.internshipRepository
      .createQueryBuilder('intership')
      .where('schoolSupervisor1Id = :id OR schoolSupervisor2Id = :id', {
        id: idTeacher,
      })
      .getMany();
    return {
      interships: interships,
    };
  }

  //SELECT * FROM `internship` WHERE `soutenanceId` IS NULL AND `endDate` < '2023-12-01'
  async GetPossibleInternshipsForDate(soutenanceDate) {
    const interships = await this.internshipRepository
      .createQueryBuilder('intership')
      .where('soutenanceId IS NULL AND `endDate` < :date', {
        date: soutenanceDate,
      })
      .getMany();
    return {
      interships: interships,
    };
  }
}
