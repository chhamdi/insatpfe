import { ApiProperty } from '@nestjs/swagger';
import { StudentDto } from '../../student/DTO/student.dto';
import { TeacherDto } from '../../teacher/DTO/teacher.dto';

export class InternshipDto {
  @ApiProperty({
    type: String,
    description: "l'étudiant",
  })
  student: StudentDto;

  @ApiProperty({
    type: String,
    description: 'le sujet',
  })
  topic: string;

  @ApiProperty({
    type: String,
    description: 'le rapport',
  })
  report: string;

  @ApiProperty({
    type: String,
    description: 'la société',
  })
  society: string;

  @ApiProperty({
    type: String,
    description: "l'encadreur dans l'entreprise",
  })
  societySupervisor: string;

  @ApiProperty({
    type: String,
    description: "le premier encadreur dans l'université",
  })
  schoolSupervisor1: TeacherDto;

  @ApiProperty({
    type: String,
    description: "le deuxiéme encadreur dans l'université",
  })
  schoolSupervisor2: TeacherDto;

  @ApiProperty({
    type: String,
    description: 'la date début de stage',
  })
  startDate: Date;

  @ApiProperty({
    type: String,
    description: 'la date fin de stage',
  })
  endDate: Date;

  @ApiProperty({
    type: String,
    description: "l'état",
  })
  state: string;
}
