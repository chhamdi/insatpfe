import { JwtAuthGuard } from './../../user/Guards/jwt-auth.guard';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { StateEnum } from 'src/internship/enums/stage-state.enum';
import { InternshipService } from '../services/internship.service';
import { InternshipDto } from '../DTO/internship.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';
import { ApiTags } from '@nestjs/swagger';
import { Timestamp } from 'typeorm';

@ApiTags('stages')
@Controller('stages')
export class InternshipController {
  constructor(private internshipService: InternshipService) {}

  @Post()
  //@UseGuards(JwtAuthGuard)
  async addInternship(@Body() newInternship: InternshipDto) {
    return await this.internshipService.create(newInternship);
  }

  @Post(':id/valider')
  //@UseGuards(JwtAuthGuard)
  async validateInternship(@Param('id') idInternship) {
    const internship = await this.internshipService.findInternshipById(
      idInternship,
    );
    internship.state = StateEnum.VALID;
    return this.internshipService.create(internship);
  }

  @Post(':id/refuser')
  //@UseGuards(JwtAuthGuard)
  async rejectInternship(@Param('id') idInternship: string) {
    const internship = await this.internshipService.findInternshipById(
      idInternship,
    );
    internship.state = StateEnum.REFUSED;
    return await this.internshipService.create(internship);
  }
  @Delete(':id')
  //@UseGuards(JwtAuthGuard)
  deleteInternship(@Param('id') id: string) {
    return this.internshipService.delete(id);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.internshipService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 10 ? 10 : paginationDto.limit,
    });
  }

  @Get('soutenances/:date')
  async GetPossibleInternshipsForDate(
    @Param('date') soutenanceDate: Timestamp,
  ) {
    return this.internshipService.GetPossibleInternshipsForDate(soutenanceDate);
  }
}
