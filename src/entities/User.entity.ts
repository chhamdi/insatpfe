import { AccountStateEnum } from './../enums/account-state.enum';
import { UserRoleEnum } from 'src/enums/user-role.enum';
import { Entity, Column, PrimaryGeneratedColumn} from 'typeorm';


  @Entity('user')
  export class UserEntity {
  
    @PrimaryGeneratedColumn()
    id: string;

    @Column({
      type: 'enum',
      enum: UserRoleEnum,
      default: UserRoleEnum.ADMIN
    })
    role: string;

    @Column({
      length: 50,
      unique: true
    })
    username: string;
  
    @Column({
      unique: true
    })
    email: string;
  
    @Column()
    password: string;

    @Column()
    name: string;

    @Column()
    firstname: string;

    @Column()
    cin: number;

    @Column()
    passeport: number;

    @Column()
    telephone: number;

    @Column({
      type: 'enum',
      enum: AccountStateEnum,
      default: AccountStateEnum.ACTIVATED
    })
    valid: boolean;
  
    //@Column()
    //salt: string; 
  }

