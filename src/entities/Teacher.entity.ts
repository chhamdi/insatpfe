import { Entity, OneToMany } from 'typeorm';
import { SoutenanceEntity } from './Soutenance.entity';
import { InternshipEntity } from './Internship.entity';
import { UserEntity } from './User.entity';


  @Entity('teacher')
  export class TeacherEntity extends UserEntity{
      
    internshipSupervisedList: InternshipEntity[];

    supervisorSoutenanceList: SoutenanceEntity[];

    @OneToMany(
        type => SoutenanceEntity,
        (soutenance) => soutenance.president,
        {lazy: true}
    )
    presidentSoutenanceList: SoutenanceEntity[];

    @OneToMany(
        type => SoutenanceEntity,
        (soutenance) => soutenance.reviewer,
        {lazy: true}
    )
    reviewerSoutenanceList: SoutenanceEntity[];

  }

