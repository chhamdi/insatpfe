import { StateEnum } from "src/enums/stage-state.enum";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, Repository } from 'typeorm';
import { TeacherEntity } from './Teacher.entity';
import { StudentEntity } from './Student.entity';
import { SoutenanceEntity } from "./Soutenance.entity";



@Entity('internship')
export class InternshipEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
      type => StudentEntity,
      (student) => student.internshipList,
      {eager: false}
    )
  student: StudentEntity;

  @OneToOne(type => SoutenanceEntity,{eager: true})
  @JoinColumn()
  soutenance: SoutenanceEntity;
  
  @Column()
  topic: string;

  @Column()
  report: string;

  @Column()
  society: string;

  @Column()
  societySupervisor: string;

  @ManyToOne(
    type => TeacherEntity,
    (teacher )=> teacher.internshipSupervisedList,
    {eager: true}
  )
  schoolSupervisor1:TeacherEntity;

  @ManyToOne(
    type => TeacherEntity,
    (teacher )=> teacher.internshipSupervisedList,
    {eager: true}
  )
  schoolSupervisor2:TeacherEntity;

  @Column()
  startDate: Date;

  @Column()
  endDate: Date;

  @Column({
      type: 'enum',
      enum: StateEnum,
      default: StateEnum.WAITING
    })
  state: string;

}