import { Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { StudentEntity } from './Student.entity';
import { TeacherEntity } from './Teacher.entity';
import { SessionEntity } from "./Session.entity";
import { InternshipEntity } from './Internship.entity';
import { RoomEntity } from './Room.entity';



  @Entity('soutenance')
  export class SoutenanceEntity {
  
    @PrimaryGeneratedColumn()
    id: string;

    @ManyToOne(
        type => SessionEntity,
        (session) => session.soutenanceList,
        {eager: true}
    )
    session: SessionEntity;  


    @ManyToOne(
        type => TeacherEntity,
        (teacher) => teacher.presidentSoutenanceList,
        {eager: true}
    )
    president: TeacherEntity;

    @ManyToOne(
        type => TeacherEntity,
        (teacher) => teacher.reviewerSoutenanceList,
        {eager: true}
    )
    reviewer: TeacherEntity;

    @Column()
    soutenanceStartTime: Date;

    @Column()
    soutenanceEndTime: Date;
    
    @ManyToOne(
        type => RoomEntity,
        (room) => room.soutenancesList,
        {eager: true}
    )
    room: RoomEntity;
}