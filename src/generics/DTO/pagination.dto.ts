import { ApiProperty } from '@nestjs/swagger';

export class PaginationDto {
  @ApiProperty({
    type: Number,
    description: 'le numéro de page',
  })
  page: number;

  @ApiProperty({
    type: Number,
    description: "le nombre d'élèments par page",
  })
  limit: number;
}
