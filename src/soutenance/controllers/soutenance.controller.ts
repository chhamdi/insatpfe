import { JwtAuthGuard } from './../../user/Guards/jwt-auth.guard';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';
import { SoutenanceDto } from '../DTO/soutenance.dto';
import { SoutenanceService } from '../services/soutenance.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('soutenances')
@Controller('soutenances')
export class SoutenanceController {
  constructor(private soutenanceService: SoutenanceService) {}

  @Post()
  //@UseGuards(JwtAuthGuard)
  async addSoutenance(@Body() newSoutenance: SoutenanceDto) {
    return await this.soutenanceService.create(newSoutenance);
  }

  @Get(':id')
  async getSoutenance(@Param('id') idSoutenance: string) {
    return await this.soutenanceService.findSoutenanceById(idSoutenance);
  }

  @Delete(':id')
  //@UseGuards(JwtAuthGuard)
  deleteSoutenance(@Param('id') id: string) {
    return this.soutenanceService.delete(id);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.soutenanceService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 10 ? 10 : paginationDto.limit,
    });
  }
}
