import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SoutenanceEntity } from './entities/Soutenance.entity';
import { SoutenanceService } from './services/soutenance.service';
import { SoutenanceController } from './controllers/soutenance.controller';

@Module({
  controllers: [SoutenanceController],
  providers: [SoutenanceService],
  imports: [
    TypeOrmModule.forFeature([
        SoutenanceEntity
    ])
  ]
})
export class SoutenanceModule {}