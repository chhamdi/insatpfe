import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { TeacherEntity } from '../../teacher/entities/Teacher.entity';
import { SessionEntity } from '../../session/entities/Session.entity';
import { RoomEntity } from '../../room/entities/Room.entity';

@Entity('soutenance')
export class SoutenanceEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @ManyToOne((_type) => SessionEntity, (session) => session.soutenanceList, {
    eager: true,
  })
  session: SessionEntity;

  @ManyToOne(
    (_type) => TeacherEntity,
    (teacher) => teacher.presidentSoutenanceList,
    { eager: true },
  )
  president: TeacherEntity;

  @ManyToOne(
    (_type) => TeacherEntity,
    (teacher) => teacher.reviewerSoutenanceList,
    { eager: true },
  )
  reviewer: TeacherEntity;

  @Column()
  soutenanceStartTime: Date;

  @Column()
  soutenanceEndTime: Date;

  @ManyToOne((_type) => RoomEntity, (room) => room.soutenancesList, {
    eager: true,
  })
  room: RoomEntity;
}
