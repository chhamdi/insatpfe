import { ApiProperty } from '@nestjs/swagger';
import { InternshipDto } from '../../internship/DTO/internship.dto';
import { RoomDto } from '../../room/DTO/room.dto';
import { SessionDto } from '../../session/DTO/session.dto';
import { TeacherDto } from '../../teacher/DTO/teacher.dto';

export class SoutenanceDto {
  @ApiProperty({
    type: String,
    description: 'la session',
  })
  session: SessionDto;

  @ApiProperty({
    type: String,
    description: 'le stage',
  })
  internship: InternshipDto;
  @ApiProperty({
    type: String,
    description: "l'année academique",
  })
  president: TeacherDto;

  @ApiProperty({
    type: String,
    description: "l'encadreur",
  })
  reviewer: TeacherDto;
  @ApiProperty({
    type: String,
    description: 'le premier professeur encadreur',
  })
  schoolSupervisor1: TeacherDto;

  @ApiProperty({
    type: String,
    description: 'le deuxiéme professeur encadreur',
  })
  schoolSupervisor2: TeacherDto;
  @ApiProperty({
    type: Date,
    description: 'la date de début soutenance',
  })
  soutenanceStartTime: Date;

  @ApiProperty({
    type: Date,
    description: 'la date de la fin soutenance',
  })
  soutenanceEndTime: Date;

  @ApiProperty({
    type: String,
    description: '',
  })
  room: RoomDto;
}
