import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SoutenanceEntity } from '../entities/Soutenance.entity';
import { Repository } from 'typeorm';
import { SoutenanceDto } from '../DTO/soutenance.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';

@Injectable()
export class SoutenanceService {
  constructor(
    @InjectRepository(SoutenanceEntity)
    private soutenanceRepository: Repository<SoutenanceEntity>,
  ) {}

  async create(newSoutenance: SoutenanceDto): Promise<SoutenanceEntity> {
    return await this.soutenanceRepository.save(newSoutenance);
  }

  async findSoutenanceById(id: string) {
    return await this.soutenanceRepository.findOne(id);
  }

  async delete(id: string) {
    return await this.soutenanceRepository.delete(id);
  }

  async findAll(paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;

    const totalCount = await this.soutenanceRepository.count();
    const soutenances = await this.soutenanceRepository
      .createQueryBuilder()
      .orderBy('id', 'DESC')
      .offset(skippedItems)
      .limit(paginationDto.limit)
      .getMany();

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      data: soutenances,
    };
  }

  async GetListOfSoutenanceWhereTeacherIsPresident(idTeacher: string) {
    const soutenances = await this.soutenanceRepository
      .createQueryBuilder()
      .where('presidentId = :id', {
        id: idTeacher,
      })
      .getMany();
    return {
      soutenances: soutenances,
    };
  }

  async GetListOfSoutenanceWhereTeacherIsReview(idTeacher: string) {
    const soutenances = await this.soutenanceRepository
      .createQueryBuilder()
      .where('reviewerId = :id', {
        id: idTeacher,
      })
      .getMany();
    return {
      soutenances: soutenances,
    };
  }
}
