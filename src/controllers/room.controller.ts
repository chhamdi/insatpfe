import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { RoomEntity } from 'src/entities/Room.entity';
import { RoomService } from 'src/services/room.service';
import { RoomDto } from '../DTO/room.dto';

@Controller('salles')
export class RoomController {
    constructor(private roomService: RoomService ) {}

    @Post()
    async addRoom(
    @Body() newRoom: RoomDto): Promise<RoomEntity> {
        return await this.roomService.create(newRoom);
    }

    @Get(':id')
    async getRoom(
    @Param('id') idRoom) {
        return await this.roomService.findRoomById(idRoom);
    }

}
