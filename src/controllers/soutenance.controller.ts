import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { SoutenanceDto } from 'src/DTO/soutenance.dto';
import { SoutenanceEntity } from 'src/entities/Soutenance.entity';
import { SoutenanceService } from 'src/services/soutenance.service';

@Controller('soutenances')
export class SoutenanceController {
    
    constructor(private soutenanceService: SoutenanceService ) {}

    @Post()
    async addSoutenance(
    @Body() newSoutenance: SoutenanceDto) {
        return await this.soutenanceService.create(newSoutenance);
    }

    @Get(':id')
    async getSoutenance(
    @Param('id') idSoutenance) {
        return await this.soutenanceService.findSoutenanceById(idSoutenance);
    }
  
}
