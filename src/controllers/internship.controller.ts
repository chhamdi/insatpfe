import { Body, Controller, Param, Post } from '@nestjs/common';
import { StateEnum } from 'src/enums/stage-state.enum';
import { InternshipService } from 'src/services/internship.service';
import { InternshipDto } from '../DTO/internship.dto';

@Controller('stages')
export class InternshipController {
    constructor(private internshipService: InternshipService ) {}


    @Post()
    async addInternship(
    @Body() newInternship: InternshipDto) {
        return await this.internshipService.create(newInternship);
    } 

    @Post(':id/valider')
    async validateInternship(
    @Param('id') idInternship){
        const internship = await this.internshipService.findInternshipById(idInternship);
        internship.state=StateEnum.VALID;
        return this.internshipService.create(internship)
    }

    @Post(':id/refuser')
    async rejectInternship(
    @Param('id') idInternship) {
        const internship = await this.internshipService.findInternshipById(idInternship);
        internship.state=StateEnum.REFUSED;
        return await this.internshipService.create(internship);
    }
   
}
