import { Body, Controller, Get, Param, Post, Put, Req, UploadedFile, UseInterceptors } from '@nestjs/common';
import { SoutenanceEntity } from 'src/entities/Soutenance.entity';
import { StudentService } from 'src/services/student.service';
import { StudentEntity } from '../entities/Student.entity';
import { StudentDto } from '../DTO/student.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('etudiants')
export class StudentController {

    constructor(private studentService: StudentService ) {}

    @Post('/fromFile')
    @UseInterceptors(FileInterceptor('file'))
    async addFromFile(
    @UploadedFile() file: any) {
        console.log(typeof(file.buffer))
        console.log(Object.keys(file["buffer"]))
        return {"req": file} ;
    }

    @Post()
    async addStudent(
    @Body() newStudent: StudentDto) {
        return await this.studentService.create(newStudent);
    }

    @Get(':id')
    async getStudent(
    @Param('id') idStudent):Promise<StudentEntity> {
        return await this.studentService.findStudentById(idStudent);
    }

    @Get(':id/stages')
    async getListOfInternshipOfStudent(idStudent){
        const student = await this.studentService.findStudentById(idStudent);
        return student.internshipList;  
    }
    
    @Get(':id/annee')
    async getYearOfStudent(idStudent){
        const student = await this.studentService.findStudentById(idStudent);
        return student.academicYear;  
    }
    @Get(':id/soutenances')
    async getListOfSoutenanceOfStudent(idStudent){
        const listOfInternships = await this.getListOfInternshipOfStudent(idStudent);
        var listOfSoutenances : SoutenanceEntity []=[];
        for (let internship of listOfInternships ) {
            if(internship.soutenance!=null){
                listOfSoutenances.push(internship.soutenance)
            }
        }
        return listOfSoutenances; 
    }
   
}
