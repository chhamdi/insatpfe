import { Body, Controller, Post, Get, Param } from '@nestjs/common';
import { AcademicYearService } from 'src/services/academic-year.service';
import { AcademicYearDto } from '../DTO/academicYear.dto';

@Controller('annees_universitaires')
export class AcademicYearController {

    constructor(private academicYearService: AcademicYearService ) {}

    @Post()
    async addAcademicYear(
    @Body() newAcademicYear: AcademicYearDto) {
        return await this.academicYearService.create(newAcademicYear);
    }

    @Get(':id')
    async readAcademicYear(
    @Param('id') idAcademicYear) {
        return await this.academicYearService.findAcademicYearById(idAcademicYear);
    }

}
