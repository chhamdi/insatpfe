import { SessionDto } from './../DTO/session.dto';
import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { SessionEntity } from 'src/entities/Session.entity';
import { SessionService } from 'src/services/session.service';

@Controller('sessions')
export class SessionController {
    constructor(private sessionService: SessionService ) {}

    @Post()
    async addSession(
    @Body() newSession: SessionDto): Promise<SessionEntity> {
        return await this.sessionService.create(newSession);
    }

    @Get(':id')
    async getSession(
    @Param('id') idSession) {
        return await this.sessionService.findSessionById(idSession);
    }
}
