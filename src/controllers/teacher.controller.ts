import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { TeacherService } from 'src/services/teacher.service';
import { InternshipService } from '../services/internship.service';
import { TeacherDto } from '../DTO/teacher.dto';

@Controller('enseignants')
export class TeacherController {
    constructor(private teacherService: TeacherService, private internshipService: InternshipService) {}

    @Post()
    async addTeacher(
    @Body() newTeacher: TeacherDto) {
        return await this.teacherService.create(newTeacher);
    }

    @Get(':id')
    async getTeacher(
    @Param('id') idTeacher) {
        return await this.teacherService.findTeacherById(idTeacher);
    }

}
