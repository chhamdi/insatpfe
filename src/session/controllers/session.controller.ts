import { JwtAuthGuard } from './../../user/Guards/jwt-auth.guard';
import { SessionDto } from '../DTO/session.dto';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { SessionEntity } from '../entities/Session.entity';
import { SessionService } from '../services/session.service';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('sessions')
@Controller('sessions')
export class SessionController {
  constructor(private sessionService: SessionService) {}

  @Post()
  //@UseGuards(JwtAuthGuard)
  async addSession(@Body() newSession: SessionDto): Promise<SessionEntity> {
    return await this.sessionService.create(newSession);
  }

  @Get(':id')
  async getSession(@Param('id') idSession: string) {
    return await this.sessionService.findSessionById(idSession);
  }
  @Delete(':id')
  //@UseGuards(JwtAuthGuard)
  deleteSession(@Param('id') id: string) {
    return this.sessionService.delete(id);
  }

  @Get()
  findAll(@Query() paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    paginationDto.page = Number(paginationDto.page);
    paginationDto.limit = Number(paginationDto.limit);

    return this.sessionService.findAll({
      ...paginationDto,
      limit: paginationDto.limit > 10 ? 10 : paginationDto.limit,
    });
  }
}
