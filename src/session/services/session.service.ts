import { SessionEntity } from '../entities/Session.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SessionDto } from '../DTO/session.dto';
import { PaginationDto } from 'src/generics/DTO/pagination.dto';
import { PaginatedResultDto } from 'src/generics/DTO/paginationResult.dto';

@Injectable()
export class SessionService {
    constructor(
        @InjectRepository(SessionEntity)
        private sessionRepository: Repository<SessionEntity>
    ){}
    
    async create(newSession: SessionDto): Promise<SessionEntity> {
        return await this.sessionRepository.save(newSession);
    }

    async findSessionById(id: string) {
        return await this.sessionRepository.findOne(id);
   } 
   async delete(id: string) {
    return await this.sessionRepository.delete(id);
   }

async findAll(paginationDto: PaginationDto): Promise<PaginatedResultDto> {
    const skippedItems = (paginationDto.page - 1) * paginationDto.limit;

    const totalCount = await this.sessionRepository.count()
    const sessions = await this.sessionRepository.createQueryBuilder()
      .orderBy('id', "DESC")
      .offset(skippedItems)
      .limit(paginationDto.limit)
      .getMany()

    return {
      totalCount,
      page: paginationDto.page,
      limit: paginationDto.limit,
      data: sessions,
    }
}
}
