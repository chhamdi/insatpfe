import { Column, Entity, PrimaryGeneratedColumn, OneToMany, ManyToOne } from 'typeorm';
import { AcademicYearEntity } from '../../academic-year/entities/AcademicYear.entity';
import { SoutenanceEntity } from '../../soutenance/entities/Soutenance.entity';


  @Entity('session')
  export class SessionEntity {

    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    startDate: Date;

    @Column()
    endDate: Date;

    @OneToMany(
        type => SoutenanceEntity,
        (soutenance) => soutenance.session
        //{lazy: true}
    )
    soutenanceList: SoutenanceEntity [];

    @ManyToOne(
      type => AcademicYearEntity,
      (year) => year.sessionList,
      {eager: true}
    )
    academicYear: AcademicYearEntity;
  
}