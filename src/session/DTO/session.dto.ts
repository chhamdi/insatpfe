import { ApiProperty } from '@nestjs/swagger';
import { AcademicYearDto } from '../../academic-year/DTO/academicYear.dto';

export class SessionDto {
  @ApiProperty({
    type: String,
    description: "la date de début d'une session",
  })
  startDate: Date;

  @ApiProperty({
    type: String,
    description: "la date de la fin d'une session",
  })
  endDate: Date;

  @ApiProperty({
    type: String,
    description: "l'année academique",
  })
  academicYear: AcademicYearDto;
}