import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SessionService } from './services/session.service';
import { SessionController } from './controllers/session.controller';
import { SessionEntity } from './entities/Session.entity';

@Module({
  controllers: [SessionController],
  providers: [SessionService],
  imports: [
    TypeOrmModule.forFeature([
        SessionEntity
    ])
  ]
})
export class SessionModule {}