import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
export class LoginCredentialsDto {

  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: '',
  })
  username: string;

  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: 'le mot de passe',
  })
  password: string;
}
