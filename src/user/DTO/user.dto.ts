import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumber } from 'class-validator';

export class UserDto {
  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: '',
  })
  username: string;

  @IsEmail()
  @ApiProperty({
    type: String,
    description: "l'email",
  })
  email: string;

  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: 'le mot de passe',
  })
  password: string;

  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: 'le nom',
  })
  name: string;

  @IsNotEmpty()
  @ApiProperty({
    type: String,
    description: 'le prénom',
  })
  firstname: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    type: Number,
    description: "le numéro de carte d'identité national",
  })
  cin: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    type: Number,
    description: 'le numéro de passport',
  })
  passeport: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({
    type: Number,
    description: 'le numéro de télèphone',
  })
  telephone: number;
}
