import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserDto } from '../DTO/user.dto';
import { UserEntity } from '../entities/User.entity';
import * as bcrypt from 'bcrypt';
import { LoginCredentialsDto } from '../DTO/login-credentials.dto';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private jwtService: JwtService,
  ) {}

  async register(userData: UserDto): Promise<Partial<UserEntity>> {
    const user = this.userRepository.create({
      ...userData,
    });
    user.salt = await bcrypt.genSalt();
    user.password = await bcrypt.hash(user.password, user.salt);

    try {
      await this.userRepository.save(user);
    } catch (e) {
      throw new ConflictException("L'email et le password doivent être unique");
    }

    delete user.salt;
    delete user.password;
    return user;
  }

  async login(credentials: LoginCredentialsDto) {
    // Récupérer le username + pwd
    const { username, password } = credentials;
    // vérifier s'il y a un user avec ce username
    const user = await this.userRepository.findOne({ username });

    // Si non username ou pwd innexistant
    if (!user) {
      // Si non username ou pwd innexistant
      throw new NotFoundException('username ou password erroné');
    } else {
      // Si oui
      // Je hash le password avec le salt du user

      // const hashedPassword = bcrypt.hash(password, users[0].salt);
      // si egal au pwd du user return ok
      // if (hashedPassword === users[0].password) {
      if (await bcrypt.compare(password, user.password)) {
        const payload = {
          username: user.username,
          email: user.email,
          role: user.role,
        };
        const jwt = this.jwtService.sign(payload);
        return {
          access_token: jwt,
        };
      } else {
        throw new NotFoundException('username ou password erroné');
      }
      // sinon
      //username ou pwd innexistant
    }
  }
}
