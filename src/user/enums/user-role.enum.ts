export enum UserRoleEnum {
    ADMIN = 'admin',
    ETUDIANT = 'etudiant',
    ENSEIGNANT='enseignant'
  }