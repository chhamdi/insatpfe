import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './entities/User.entity';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import * as dotenv from 'dotenv';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { PassportJwtStrategy } from './strategy/passport-jwt.strategy';

dotenv.config();

@Module({
  controllers: [UserController],
  providers: [UserService, PassportJwtStrategy],
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.SECRET,
      signOptions: {
        expiresIn: 36000,
      },
    }),
  ],
})
export class UserModule {}
