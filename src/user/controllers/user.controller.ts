import { LoginCredentialsDto } from './../DTO/login-credentials.dto';
import { UserDto } from './../DTO/user.dto';
import { Body, Controller, Post } from '@nestjs/common';
import { UserEntity } from '../entities/User.entity';
import { UserService } from '../services/user.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('authentification')
@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Post('register')
  register(@Body() userData: UserDto): Promise<Partial<UserEntity>> {
    return this.userService.register(userData);
  }

  @Post('login')
  login(@Body() credentials: LoginCredentialsDto) {
    return this.userService.login(credentials);
  }
}
