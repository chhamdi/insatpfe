FROM node:12-alpine as shellprovider

RUN /bin/sh -c "apk add --no-cache bash"
RUN apk update && apk --no-cache add --virtual builds-deps build-base python


FROM node:12-alpine as builder

COPY package.json .
RUN npm install
COPY . .
RUN npm run build


FROM node:12-alpine

COPY --from=builder ./dist ./dist
COPY --from=builder ./node_modules ./node_modules
EXPOSE 3000
CMD ["node", "dist/main"]